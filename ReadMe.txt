Building instructions:

1) Download packages from assets store:

Industrial Textures 1.0 - Arkham Interactive
Prototyping Pack (Free) 1.0 - DigitalKonstrukt
Sci-Fi Textures Pack 1 V2.0 - FireBolt Studios
Free PBR Lamps - New Solution Studio
Props for the classroom - VR

2) Build for your architecture
3) Copy the scan data from: https://goo.gl/moTZwH Data folder into buildxx_Data/Resources folder
4) Run