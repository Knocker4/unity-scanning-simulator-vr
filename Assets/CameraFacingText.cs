﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFacingText : MonoBehaviour
{
    public GameObject _mainCamera;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(_mainCamera.transform);
        transform.Rotate(Vector3.up - new Vector3(0, 180, 0));
	}
}
