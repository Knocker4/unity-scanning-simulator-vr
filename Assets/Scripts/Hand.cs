﻿using System;
using Assets.Scripts.Interactable;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts
{
    public class Hand : MonoBehaviour
    {
        public enum State
        {
            EMPTY,
            TOUCHING,
            HOLDING
        };

        private const String INTERACTABLE_LAYER = "InteractableObject";

        public OVRInput.Controller Controller = OVRInput.Controller.LTouch;
        public State HandState = State.EMPTY;
        public bool IgnoreContactPoint = false;

        private Rigidbody _attachPoint = null;
        private FixedJoint _tempJoint;
        [SerializeField] private IInteractableObject _interactableObject;
        [SerializeField] private Boolean _shouldForgetObject = false;


        void Start()
        {
            if (_attachPoint == null)
            {
                _attachPoint = GetComponent<Rigidbody>();
            }
        }

        void OnTriggerEnter(Collider collider)
        {
            //Debug.Log("Entered collider of: " + collider.gameObject.name);

            _shouldForgetObject = false;
            if (HandState == State.EMPTY || HandState == State.TOUCHING)
            {
                var temp = collider.gameObject;
                if (temp != null && temp.layer == LayerMask.NameToLayer(INTERACTABLE_LAYER) &&
                    temp.GetComponent<IInteractableObject>() != null)
                {
                    _interactableObject = temp.GetComponent<IInteractableObject>();
                    HandState = State.TOUCHING;
                }
            }
        }

        void OnTriggerExit(Collider collider)
        {
            if (collider.gameObject.layer == LayerMask.NameToLayer(INTERACTABLE_LAYER))
            {
                //Debug.Log("Left collider of: " + collider.gameObject.name);

                if (HandState != State.HOLDING)
                {
                    _interactableObject = null;
                    HandState = State.EMPTY;
                }
                else
                {
                    _shouldForgetObject = true;
                }
            }
        }


        void Update()
        {
            switch (HandState)
            {
                case State.TOUCHING:
                    if (_interactableObject != null && OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, Controller) >=
                        0.5f)
                    {
                        _interactableObject.StartInteraction(this.gameObject);
                        HandState = State.HOLDING;
                    }
                    break;
                case State.HOLDING:
                    if (_interactableObject != null)
                    {
                        if (OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, Controller) < 0.5f)
                        {
                            _interactableObject.StopInteraction();
                            if (_shouldForgetObject)
                                _interactableObject = null;
                            if (_tempJoint != null)
                            {
                                Object.DestroyImmediate(_tempJoint);
                                _tempJoint = null;
                            }
                            HandState = State.TOUCHING;
                        }
                    }
                    break;
            }
        }

        public void AttachBody(FixedJoint tempJoint)
        {
            _tempJoint = tempJoint;
            _tempJoint.connectedBody = _attachPoint;
        }
    }
}