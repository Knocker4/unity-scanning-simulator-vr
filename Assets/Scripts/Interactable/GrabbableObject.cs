﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using Assets.Scripts.Interactable;
using UnityEngine;

public class GrabbableObject : IInteractableObject
{
    private FixedJoint _joint;

    public override void Interact()
    {
        _joint = this.gameObject.AddComponent<FixedJoint>();
        _controllerObject.GetComponent<Hand>().AttachBody(_joint);
    }

    public override void StopInteraction() {
        //donothing 
        //TODO: implement throw here if needed.
    }

    private void throwObject()
    {
        //            mHeldObject.velocity = OVRInput.GetLocalControllerVelocity(Controller);
        //             mHeldObject.angularVelocity = OVRInput.GetLocalControllerAngularVelocity(Controller).eulerAngles * Mathf.Deg2Rad;
        //            mHeldObject.maxAngularVelocity = mHeldObject.angularVelocity.magnitude;
    }
}
