﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Channels;
using Assets.Scripts;
using Assets.Scripts.Interactable;
using Assets.Scripts.PlyReader;
using UnityEngine;

public class PressableObject : IInteractableObject
{

    private const String PUSH_TRIGGER = "ButtonPushed";

//    public PlyReader PointCloudManager;

    public SceneManager _sceneManager;
    private int i = 0;

    private SceneManager.ButtonType tagToButtonType()
    {
        switch (gameObject.tag)
        {
            case "SCAN":
            {
                return SceneManager.ButtonType.SCAN;
            }
            case "ROTATE_INC":
            {
                return SceneManager.ButtonType.ROTATE_INC;
            }
            case "ROTATE_DEC":
            {
                return SceneManager.ButtonType.ROTATE_DEC;
            }
            default: return SceneManager.ButtonType.NONE;
        }
    }


    public override void Interact()
    {
        this.GetComponent<Animator>().SetTrigger(PUSH_TRIGGER);
        _sceneManager.ButtonPressed(tagToButtonType());
    }

    public override void StopInteraction()
    {
        // donothing
    }
}
