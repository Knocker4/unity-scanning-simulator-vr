﻿using UnityEngine;

namespace Assets.Scripts.Interactable
{
    public class HandRotateableObject : IInteractableObject
    {

        private Quaternion _initialControllerRotation;
        private bool _isRotating = false;
        private OVRInput.Controller _controller;


        // Update is called once per frame
        void FixedUpdate()
        {
            if (_isRotating)
            {
                RigidbodyConstraints constraints = gameObject.GetComponent<Rigidbody>().constraints;
                Quaternion rotation = OVRInput.GetLocalControllerRotation(_controller);
                rotation.x -= _initialControllerRotation.x;
                rotation.y -= _initialControllerRotation.y;
                rotation.z -= _initialControllerRotation.z;

                if ((constraints & RigidbodyConstraints.FreezeRotationX) == RigidbodyConstraints.FreezeRotationX)
                {
                    rotation.x = 0;
                    //rotation.x = 0;
                }
                if ((constraints & RigidbodyConstraints.FreezeRotationY) == RigidbodyConstraints.FreezeRotationY)
                {
                    rotation.y = 0;
                    //rotation.y = 0;
                }
                if ((constraints & RigidbodyConstraints.FreezeRotationZ) == RigidbodyConstraints.FreezeRotationZ)
                {
                    rotation.z = 0;
                    //rotation.z = 0;
                }
                
                transform.rotation *= rotation;
                _initialControllerRotation = OVRInput.GetLocalControllerRotation(_controller);
            }
        }

        public override void Interact()
        {
            _isRotating = true;
            _controller = _controllerObject.GetComponent<Hand>().Controller;
            _initialControllerRotation = OVRInput.GetLocalControllerRotation(_controller);
        }

        public override void StopInteraction()
        {
            _isRotating = false;
        }
    }
}
