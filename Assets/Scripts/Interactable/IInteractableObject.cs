﻿using UnityEngine;

namespace Assets.Scripts.Interactable
{
    public abstract class IInteractableObject : MonoBehaviour
    {
        protected GameObject _controllerObject;
        
        public void StartInteraction(GameObject controllerObject) {
            _controllerObject = controllerObject;
            Interact();
        }

        public abstract void Interact();

        public abstract void StopInteraction();

    }
}
