﻿using System;
using UnityEngine;

namespace Assets.Scripts.Interactable
{
    public class RotateableObject : IInteractableObject {

        [SerializeField]
        private float _sensitivity = 2.0f;
        [SerializeField]
        private int _frameRate = 100;
        private Vector3 _controllerReference;
        private Vector3 _controllerOffset;
        private Vector3 _rotation;
        private bool _isRotating;
        private OVRInput.Controller _controller;

        private int i = 0;

        void Start() {
            _rotation = Vector3.zero;
            _controllerReference = Vector3.zero;
            _isRotating = false;
        }

        void FixedUpdate() {
            if (_isRotating) {

                _controllerOffset = OVRInput.GetLocalControllerPosition(_controller) - _controllerReference;
                _rotation.y = -(_controllerOffset.x + _controllerOffset.y) * _sensitivity;
                transform.Rotate(_rotation);

                if (i % _frameRate == 0) {
                    _controllerReference = OVRInput.GetLocalControllerPosition(_controller);
                }
                i = (i + 1) % _frameRate;

            }
        }


        private void StartRotating(OVRInput.Controller controller) {
            _isRotating = true;
            _controller = controller;
            _controllerReference = OVRInput.GetLocalControllerPosition(controller);
        }


        private void StopRotating() {
            _isRotating = false;
            _controllerReference = Vector3.zero;
        }

        public override void Interact() {
            StartRotating(_controllerObject.GetComponent<Hand>().Controller);
        }

        public override void StopInteraction()
        {
            StopRotating();
        }
    }
}
