﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Assets.Scripts.Interactable;
using UnityEngine;

namespace Assets.Scripts.PlyReader
{
    public class PlyReader : GrabbableObject{

        public Material MatVertex;
        public GameObject Anchor;

        private GameObject _pointCloud;
        private MeshData _meshData;
        private string _fileName = "Assets/Resources/ScanData/BunnyScan/PointClouds/pointcloud_1.ply";
        public float _calculatedScale = 1f;
        private Boolean _shouldCalculateMass = true;
        private Vector3 _centerPoint = Vector3.zero;
        private long _accumulatedMass = 1;

        void Start()
        {
            _centerPoint = Vector3.zero;
        }

        void Update()
        {}

        public void ReadPly(string fileName)
        {
            _fileName = fileName;
//            Debug.Log("Started Reading");
            StartCoroutine("Load", _fileName);
        }

        // ReSharper disable once UnusedMember.Local
        IEnumerator Load (string filePath) {
            _meshData = new MeshData();
//            Debug.Log("Reading: " + filePath);
            Vector3 currentCenterPoint = Vector3.zero;
            
//            Debug.Log("File Exists:" + File.Exists(filePath));
            if (File.Exists(filePath))
            {

                string plyName = _fileName.Substring(_fileName.LastIndexOf('/') + 1).Split('.')[0];

                Dictionary<String, Matrix4x4> matrixMap =
                    AlnFileReader.ReadAlmFile(_fileName.Substring(0, _fileName.LastIndexOf('/')) + "/alignment.aln");

                Matrix4x4 alnMatrix = Matrix4x4.identity;
                matrixMap.TryGetValue(plyName, out alnMatrix);

                using (BinaryReader reader = new BinaryReader(File.Open(filePath, FileMode.Open)))
                {
                    int cursor = 0;
                    int length = (int) reader.BaseStream.Length;
                    string headerLine = "";
                    bool header = true;
                    int vertexCount = 0;
                    int colorDataCount = 3;
                    int index = 0;
                    int step = 0;
                    while (cursor + step < length)
                    {
                        if (header)
                        {
                            char v = reader.ReadChar();
                            if (v == '\n')
                            {
                                if (headerLine.Contains("end_header"))
                                {
                                    header = false;
                                }
                                else if (headerLine.Contains("element vertex"))
                                {
                                    string[] split = headerLine.Split();
                                    if (split.Length > 0)
                                    {
                                        vertexCount = Convert.ToInt32(split[split.Length - 1]);
                                        _meshData.vertexCount = vertexCount;
                                        _meshData.vertices = new Vector3[vertexCount];
                                        _meshData.normals = new Vector3[vertexCount];
                                        _meshData.colors = new Color[vertexCount];
                                        _meshData.curvature = new float[vertexCount];
                                    }
                                }
                                else if (headerLine.Contains("property uchar alpha"))
                                {
                                    colorDataCount = 4;
                                }
                                headerLine = "";
                            }
                            else
                            {
                                headerLine += v;
                            }
                            step = sizeof(char);
                            cursor += step;
                        }
                        else
                        {
                            if (index < vertexCount)
                            {

                                _meshData.vertices[index] = alnMatrix.MultiplyPoint3x4(new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle()));
                                
                                currentCenterPoint += _meshData.vertices[index];
                                
                                _meshData.colors[index] = new Color(
                                                                reader.ReadByte() / 255f, 
                                                                reader.ReadByte() / 255f, 
                                                                reader.ReadByte() / 255f, 
                                                                colorDataCount > 3 ?
                                                                reader.ReadByte() / 255f :
                                                                1f);
                                _meshData.normals[index] = alnMatrix.MultiplyPoint3x4(new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle()));
                                _meshData.curvature[index] = reader.ReadSingle();

                                _calculatedScale = Math.Max(_meshData.vertices[index].magnitude, _calculatedScale);


                                step = sizeof(float) * 7 + sizeof(byte) * colorDataCount;
                                cursor += step;

                                index++;
                            }
                        }

                    }

                }

            }
            
            currentCenterPoint /= _meshData.vertexCount;

            _centerPoint = (_centerPoint * _accumulatedMass + currentCenterPoint * _meshData.vertexCount) / (_accumulatedMass + _meshData.vertexCount);
            _accumulatedMass += _meshData.vertexCount;

//            Debug.Log(_accumulatedMass);
//            Debug.Log(_centerPoint);

//            Debug.Log(_meshData.ToString());

//            Debug.Log(_calculatedScale);


            var numPointGroups = Mathf.CeilToInt(_meshData.vertexCount * 1.0f / MeshData.LIMIT_POINTS * 1.0f);

            SphereCollider collider = GetComponent<SphereCollider>();

            gameObject.transform.position = Vector3.zero;
            gameObject.transform.localScale = Vector3.one;
            collider.center = Vector3.zero;
            collider.radius = 0.5f;

            _pointCloud = new GameObject(_fileName);
            _pointCloud.transform.parent = gameObject.transform;
            _pointCloud.transform.localScale = Vector3.one;
            _pointCloud.transform.position = Vector3.zero;
            _pointCloud.transform.Rotate(new Vector3(0, 0, 180f));

            for (int i = 0; i < numPointGroups - 1; i++)
            {
                InstantiateMesh(i, MeshData.LIMIT_POINTS);
            }
            InstantiateMesh(numPointGroups - 1, _meshData.vertexCount - (numPointGroups - 1) * MeshData.LIMIT_POINTS);


            gameObject.transform.localScale = new Vector3(1f / _calculatedScale, 1f / _calculatedScale, 1f / _calculatedScale);
            gameObject.transform.position = Anchor.transform.position - (_centerPoint / _calculatedScale);

            collider.center = _centerPoint;
            collider.center = new Vector3(_centerPoint.x, _centerPoint.y / 10, _centerPoint.z);
            collider.radius = _calculatedScale / 8;

            yield return null;
        }

        void InstantiateMesh(int meshInd, int nPoints)
        {
            // Create Mesh
            GameObject pointGroup = new GameObject(_fileName + meshInd);
            pointGroup.AddComponent<MeshFilter>();
            pointGroup.AddComponent<MeshRenderer>();
            pointGroup.GetComponent<Renderer>().material = MatVertex;

            pointGroup.GetComponent<MeshFilter>().mesh = _meshData.CreateMesh(meshInd, nPoints, MeshData.LIMIT_POINTS);
            pointGroup.transform.parent = _pointCloud.transform;
            pointGroup.transform.localScale = Vector3.one;
            pointGroup.transform.position = Vector3.zero;
            pointGroup.transform.Rotate(new Vector3(0, 0, 180f));

        }

        public override void StopInteraction()
        {
            base.StopInteraction();
            transform.position = Anchor.transform.position - (_centerPoint / _calculatedScale);
            transform.rotation = Quaternion.identity;
        }
    }
}
