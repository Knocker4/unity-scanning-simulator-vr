﻿using UnityEngine;

namespace Assets.Scripts.PlyReader
{
    public class MeshData {

        public Vector3[] vertices;
        public Vector3[] normals;
        public Color[] colors;
        public float[] curvature;
        public int vertexCount;
        public Bounds bounds;

        public const int LIMIT_POINTS = 65000;
        private Vector3 minValue = Vector3.zero;

        public override string ToString()
        {
            return "Count: " + vertexCount + " Vertices:" + vertices;
        }

        public Mesh CreateMesh(int id, int nPoints, int limitPoints)
        {
            Mesh mesh = new Mesh();

            Vector3[] myPoints = new Vector3[nPoints];
            Vector3[] myNormals = new Vector3[nPoints];
            int[] indices = new int[nPoints];
            Color[] myColors = new Color[nPoints];
           
            for (int i = 0; i < nPoints; i++)
            {
                myPoints[i] = vertices[id * limitPoints + i] - minValue;
                myNormals[i] = normals[id * limitPoints + i];
                indices[i] = i;
                myColors[i] = colors[id * limitPoints + i];

            }

            mesh.vertices = myPoints;
            mesh.colors = myColors;
            mesh.SetIndices(indices, MeshTopology.Points, 0);
            mesh.uv = new Vector2[nPoints];
            mesh.normals = myNormals;


            return mesh;
        }
    }
}
