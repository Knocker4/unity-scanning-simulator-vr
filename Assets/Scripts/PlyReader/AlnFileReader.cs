﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Assets.Scripts.PlyReader
{
    public class AlnFileReader : MonoBehaviour
    {
        private static Dictionary<String, Matrix4x4> _matrixMap;

        public static Dictionary<String, Matrix4x4> ReadAlmFile(String almFilePath)
        {
            if (_matrixMap != null)
                return _matrixMap;

            _matrixMap = new Dictionary<string, Matrix4x4>();
            StreamReader reader = new StreamReader(almFilePath);

            string[] buffer = reader.ReadLine().Split();

            int numberOfClouds = int.Parse(buffer[0]);
            for (int i = 0; i < numberOfClouds; i++)
            {
                buffer = reader.ReadLine().Split('.');
                string plyName = buffer[0];
                reader.ReadLine(); // skip line with #
                Matrix4x4 matrix = new Matrix4x4();
                for (int j = 0; j < 4; j++)
                {
                    buffer = reader.ReadLine().Split();
                    matrix[j, 0] = float.Parse(buffer[0]);
                    matrix[j, 1] = float.Parse(buffer[1]);
                    matrix[j, 2] = float.Parse(buffer[2]);
                    matrix[j, 3] = float.Parse(buffer[3]);
                }
                _matrixMap.Add(plyName, matrix);
            }

//            foreach (KeyValuePair<string, Matrix4x4> keyValuePair in _matrixMap)
//            {
//                Debug.Log(keyValuePair.Key + " : " + keyValuePair.Value);
//            }

            return _matrixMap;
        }

    }
}
