﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.PlyReader;
using UnityEngine;

public class ButtonClick : MonoBehaviour
{

    public GameObject plyReader;
    private static int index = 1;

    private string fileTemplate = "Assets/Resources/ScanData/WoodenBlock/PointClouds/pointcloud_{0}.ply";

    public void ScanNext()
    {
        plyReader.GetComponent<PlyReader>().ReadPly(String.Format(fileTemplate, index));
        index++;

    }
}
