﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    public class SceneManager : MonoBehaviour
    {
        public enum ButtonType
        {
            NONE,
            SCAN,
            ROTATE_INC,
            ROTATE_DEC
        }

        public TextMesh _knobDegreeText;
        public TextMesh _scannerDegreeText;
        public GameObject _degreeKnob;
        public PlyReader.PlyReader _pointCloudManager;

        public MeshRenderer LiveCameraLeft;
        public MeshRenderer LiveCameraRight;


        private const int FRAME_RATE = 24;
        private string fileTemplate = "/Resources/ScanData/BunnyScan/PointClouds/pointcloud_{0}.ply";
        private string leftCamFile = "ScanData/BunnyScan/TrimmedSequences/sequence_{0}/Materials/frames0_0";
        private string rightCamFile = "ScanData/BunnyScan/TrimmedSequences/sequence_{0}/Materials/frames1_0";


        private static int _currentFrame = 0;
        private int _chosenRotation = 0;

        // Use this for initialization
        void Start()
        {
            fileTemplate = Application.dataPath + fileTemplate;
        }

        // Update is called once per frame
        void Update()
        {
            _chosenRotation = (int) (360-_degreeKnob.transform.rotation.eulerAngles.z) % 360;
            if (_currentFrame % FRAME_RATE == 0)
            {
                _knobDegreeText.text = _chosenRotation + "°";
                _scannerDegreeText.text = _chosenRotation + "°";

                Material leftCam =
                    Resources.Load(String.Format(leftCamFile, (_chosenRotation / 2)), typeof(Material)) as Material;
                Material rightCam =
                    Resources.Load(String.Format(rightCamFile, (_chosenRotation / 2)), typeof(Material)) as Material;

                LiveCameraLeft.materials = new Material[] {leftCam};
                LiveCameraRight.materials = new Material[] {rightCam};
            }
            _currentFrame = (_currentFrame + 1) % FRAME_RATE;
        }

        public void ButtonPressed(ButtonType buttonType)
        {
            Debug.Log("Button hit: " + buttonType);

            switch (buttonType)
            {
                case ButtonType.SCAN:
                {
                    Debug.Log("Scanning: " + String.Format(fileTemplate, (_chosenRotation / 2)));
                    _pointCloudManager.ReadPly(String.Format(fileTemplate, (_chosenRotation / 2)));
                    break;
                }
                case ButtonType.ROTATE_INC:
                {
                    _chosenRotation--;
                    _degreeKnob.transform.Rotate(new Vector3(0,
                        0, -1));
                    break;
                }
                case ButtonType.ROTATE_DEC:
                {
                    _chosenRotation++;
                    _degreeKnob.transform.Rotate(new Vector3(0,
                        0, 1));
                    break;
                }
            }
        }
    }
}